## Application Album
-1er Screen la page Album des utilisateurs. 
Il faut cliquer sur un item pour naviguer sur la page des photos.

-2eme Screen la page des photos de l'Album.

## Architecture et Code
-Clean architecture avec des paquages app, data, domaine, présentation et common.
-Réaliser en Kotlin et en jetpack compose
-Organisation de navigation

## Durée de la réalisation 
Entre 5 et 6 H

## Amélioration du projet
-Mettre un loading et gérer les errors de connection.
-Essayer de proposer une belle interface