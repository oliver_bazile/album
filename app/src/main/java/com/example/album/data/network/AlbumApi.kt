package com.example.album.data.network

import com.example.album.domain.model.Album
import com.example.album.domain.model.Photos
import com.example.album.domain.model.User
import retrofit2.Response
import retrofit2.http.GET

interface AlbumApi {

    @GET("albums")
    suspend fun getAlbums() : Response<List<Album>>

    @GET("users")
    suspend fun getUsers() : Response<List<User>>

    @GET("photos")
    suspend fun getPhotos() : Response<List<Photos>>
}