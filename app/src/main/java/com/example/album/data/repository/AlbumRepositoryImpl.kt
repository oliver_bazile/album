package com.example.album.data.repository

import com.example.album.common.Result
import com.example.album.domain.model.Album
import com.example.album.domain.model.Photos
import com.example.album.domain.model.User
import com.example.album.domain.repository.AlbumRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response

class AlbumRepositoryImpl (private val albumSource: AlbumSource) : AlbumRepository {

    private fun <T> responseToResource(response: Response<T>): Result<T> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                return Result.Success(result)
            }
        }
        return Result.Error(response.message())
    }

    override suspend fun getAlbums(): Flow<Result<List<Album>>> {
        return flow{
            emit(Result.Loading())
            emit(responseToResource(albumSource.getAlbums()))
        }
    }

    override suspend fun getUsers(): Flow<Result<List<User>>> {
        return flow{
            emit(Result.Loading())
            emit(responseToResource(albumSource.getUsers()))
        }
    }

    override suspend fun getPhotos(): Flow<Result<List<Photos>>> {
        return flow{
            emit(Result.Loading())
            emit(responseToResource(albumSource.getPhotos()))
        }
    }
}