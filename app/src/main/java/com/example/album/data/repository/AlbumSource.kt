package com.example.album.data.repository

import com.example.album.domain.model.Album
import com.example.album.domain.model.Photos
import com.example.album.domain.model.User
import retrofit2.Response

interface AlbumSource {

    suspend fun getAlbums(): Response<List<Album>>
    suspend fun getUsers(): Response<List<User>>
    suspend fun getPhotos(): Response<List<Photos>>
}