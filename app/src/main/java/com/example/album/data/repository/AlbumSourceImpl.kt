package com.example.album.data.repository

import com.example.album.data.network.AlbumApi
import com.example.album.domain.model.Album
import com.example.album.domain.model.Photos
import com.example.album.domain.model.User
import retrofit2.Response

class AlbumSourceImpl (private val albumApi: AlbumApi): AlbumSource {
    override suspend fun getAlbums(): Response<List<Album>> {
        return albumApi.getAlbums()
    }

    override suspend fun getUsers(): Response<List<User>>{
        return albumApi.getUsers()
    }


    override suspend fun getPhotos(): Response<List<Photos>> {
        return albumApi.getPhotos()
    }

}