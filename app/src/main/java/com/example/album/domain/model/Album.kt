package com.example.album.domain.model

data class Album(
    val id: Int,
    val title: String,
    val userId: Int
)