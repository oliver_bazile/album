package com.example.album.domain.model

data class Company(
    val bs: String,
    val catchPhrase: String,
    val name: String
)