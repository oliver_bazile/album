package com.example.album.domain.model

data class Geo(
    val lat: Double,
    val lng: Double
)

