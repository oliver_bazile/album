package com.example.album.domain.repository

import com.example.album.common.Result
import com.example.album.domain.model.Album
import com.example.album.domain.model.Photos
import com.example.album.domain.model.User
import kotlinx.coroutines.flow.Flow

interface AlbumRepository {

    suspend fun getAlbums() : Flow<Result<List<Album>>>
    suspend fun getUsers() : Flow<Result<List<User>>>
    suspend fun getPhotos() : Flow<Result<List<Photos>>>
}