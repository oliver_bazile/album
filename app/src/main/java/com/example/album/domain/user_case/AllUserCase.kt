package com.example.album.domain.user_case


data class AllUserCase(val getPhotos: GetPhotos, val getUsers: GetUsers,val getAlbum: GetAlbum)
