package com.example.album.domain.user_case

import com.example.album.common.Result
import com.example.album.domain.model.Album
import com.example.album.domain.repository.AlbumRepository
import kotlinx.coroutines.flow.Flow

class GetAlbum (private val albumRepository: AlbumRepository) {

    suspend fun execute(): Flow<Result<List<Album>>> {
        return albumRepository.getAlbums()
    }
}