package com.example.album.domain.user_case

import com.example.album.common.Result
import com.example.album.domain.model.Photos
import com.example.album.domain.repository.AlbumRepository
import kotlinx.coroutines.flow.Flow

class GetPhotos (private val albumRepository: AlbumRepository) {

    suspend fun execute(): Flow<Result<List<Photos>>> {
        return albumRepository.getPhotos()
    }
}