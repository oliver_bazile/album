package com.example.album.domain.user_case

import com.example.album.common.Result
import com.example.album.domain.model.User
import com.example.album.domain.repository.AlbumRepository
import kotlinx.coroutines.flow.Flow

class GetUsers (private val albumRepository: AlbumRepository) {

    suspend fun execute(): Flow<Result<List<User>>> {
        return albumRepository.getUsers()
    }
}