package com.example.album.presentation.AlbumScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.album.presentation.ui.theme.Black
import com.example.album.R
import com.example.album.presentation.ErrorCard
import com.example.album.presentation.ui.theme.TextWhite


@Composable
fun AlbumScreen(viewModel: AlbumViewModel, navigateToPhoto: (Int) -> Unit = {}) {
    LaunchedEffect(Unit) {
        viewModel.getUsers()
        viewModel.getAlbum()
    }
    val state by viewModel.state.collectAsState()
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        item {
            Text(
                text = "Liste Album",
                modifier = Modifier.fillMaxWidth().padding(top= 10.dp),
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
        }
        item { Spacer(modifier = Modifier.height(16.dp)) }
        if (state.errorAlbum.equals("") && state.errorUser.equals("")) {
            items(state.listAlbum.size) { i ->
                ItemAlbum(
                    title = state.listAlbum[i].title,
                    author = if (!state.listUsers.isNullOrEmpty()) state.listUsers[state.listAlbum[i].userId - 1].name
                    else "",
                    navigateToPhoto = navigateToPhoto,
                    albumId = state.listAlbum[i].id
                )
            }
        } else {
            item {
                ErrorCard(text = state.errorAlbum)
            }
        }
    }

}

@Composable
fun ItemAlbum(albumId: Int, title: String, author: String, navigateToPhoto: (Int) -> Unit = {}) {
    Card(
        backgroundColor = TextWhite.copy(0.2.toFloat()),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 8.dp)
            .clickable { navigateToPhoto(albumId) }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(TextWhite)
                .padding(horizontal = 10.dp, vertical = 8.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_album),
                tint = Black,
                modifier = Modifier
                    .size(80.dp)
                    .align(Alignment.CenterHorizontally),
                contentDescription = "Album"
            )
            Spacer(modifier = Modifier.height(16.dp))
            Text(text = "Titre: ${title}", style = MaterialTheme.typography.h3)
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = "Author: ${author}", style = MaterialTheme.typography.h3)
            Spacer(modifier = Modifier.height(8.dp))
        }
    }

}