package com.example.album.presentation.AlbumScreen

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.album.common.Result
import com.example.album.domain.user_case.AllUserCase
import com.example.album.domain.user_case.GetAlbum
import com.example.album.domain.user_case.GetUsers
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class AlbumViewModel @Inject constructor(
    private val app: Application,
    private var allUserCase: AllUserCase
) : AndroidViewModel(app) {

    private val _state = MutableStateFlow(AlbumViewState())
    val state: StateFlow<AlbumViewState> = _state

    fun getAlbum() {
        viewModelScope.launch {
            if (isNetworkAvailable(app)) {
                allUserCase.getAlbum.execute().collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _state.update { state ->
                                state.copy(
                                    listAlbum = result.data?.sortedBy { it.title } ?: emptyList(),
                                    loadingUser = false
                                )
                            }
                        }
                        is Result.Error -> {
                            _state.update { state ->
                                state.copy(
                                    errorUser = result.message ?: "Error "
                                )
                            }
                        }
                    }
                }
            } else {
                _state.update { state ->
                    state.copy(
                        errorAlbum = "You need to internet"
                    )
                }
            }
        }
    }

    fun getUsers() {
        viewModelScope.launch {
            if (isNetworkAvailable(app)) {
                allUserCase.getUsers.execute().collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _state.update { state ->
                                state.copy(
                                    listUsers = result.data ?: emptyList(),
                                    loadingUser = false
                                )
                            }
                        }
                        is Result.Error -> {
                            _state.update { state ->
                                state.copy(
                                    errorUser = result.message ?: "Error "
                                )
                            }
                        }
                    }
                }
            } else {
                _state.update { state ->
                    state.copy(
                        errorUser = "You need internet"
                    )
                }
            }
        }
    }

    private fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }

}