package com.example.album.presentation.AlbumScreen

import com.example.album.domain.model.Album
import com.example.album.domain.model.User

data class AlbumViewState(
    var listAlbum: List<Album> = emptyList(),
    var listUsers: List<User> = emptyList(),
    var loadingAlbum: Boolean = false,
    var loadingUser: Boolean = false,
    val errorAlbum: String = "",
    val errorUser: String = ""
)