package com.example.album.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.album.R
import com.example.album.presentation.ui.theme.Black
import com.example.album.presentation.ui.theme.TextWhite

@Composable
fun ErrorCard(text: String) {
    Card(
        elevation = 2.dp,
        backgroundColor = TextWhite,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 30.dp, vertical = 8.dp)

    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(TextWhite)
                .padding(horizontal = 10.dp, vertical = 8.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_album),
                tint = Black,
                modifier = Modifier
                    .size(80.dp)
                    .align(Alignment.CenterHorizontally),
                contentDescription = "Album"
            )
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = text, style = MaterialTheme.typography.h3,
                modifier = Modifier
                    .padding(bottom = 18.dp)
                    .align(
                        Alignment.CenterHorizontally
                    )
            )
        }
    }
}