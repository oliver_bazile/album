package com.example.album.presentation.PhotosSrceen

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.album.presentation.ErrorCard

@Composable
fun PhotosScreen(albumId: Int?, viewModel: GalleryViewModel) {

    LaunchedEffect(Unit) {
        viewModel.getPhotos(albumId = albumId ?: 1)
    }
    val state by viewModel.state.collectAsState()

    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 15.dp, vertical = 8.dp)
    ) {
        item {
            Text(
                text = "Album ${albumId}",
                modifier = Modifier.fillMaxWidth().padding(top= 10.dp),
                style = MaterialTheme.typography.h1,
                textAlign = TextAlign.Center
            )
        }
        item { Spacer(modifier = Modifier.height(16.dp)) }
        if (state.errorPhotos.equals("")) {
            items(state.listPhotos.size) { index ->
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(15.dp)
                ) {
                    AsyncImage(
                        model = state.listPhotos[index].thumbnailUrl,
                        contentDescription = null,
                        modifier = Modifier
                            .align(Alignment.Center)
                            .size(200.dp)
                    )
                }
            }
        } else {
            item {
                ErrorCard(text = state.errorPhotos)
            }
        }
    }
}