package com.example.album.presentation.PhotosSrceen

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.TextUtils.split
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.album.common.Result
import com.example.album.domain.user_case.AllUserCase
import com.example.album.domain.user_case.GetPhotos
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.util.regex.Pattern
import javax.inject.Inject

@HiltViewModel
class GalleryViewModel @Inject constructor(
    private val app: Application,
    private var allUserCase: AllUserCase
) : AndroidViewModel(app) {

    private val _state = MutableStateFlow(GalleryViewState())
    val state: StateFlow<GalleryViewState> = _state

    var array = mutableListOf(0, 0)


    fun getPhotos(albumId: Int) {
        array[0] = 1 + array.first()
        viewModelScope.launch {
            if (isNetworkAvailable(app)) {
                allUserCase.getPhotos.execute().collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _state.update { state ->
                                state.copy(
                                    listPhotos = result.data?.filter { it.albumId == albumId }
                                        ?: emptyList(),
                                    loading = false
                                )
                            }
                        }
                        is Result.Error -> {
                            _state.update { state ->
                                state.copy(
                                    errorPhotos = result.message ?: "Error "
                                )
                            }
                        }
                    }
                }
            } else {
                _state.update { state ->
                    state.copy(
                        errorPhotos = "You need to internet"
                    )
                }
            }
        }
    }

    private fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }
        return false
    }

    fun reverseWords(str:String):String{
        var value = ""
        for(i in str.length .. 0){
            value = str[i].toString()
        }
        return value
    }

    fun sumConsecutives(s: List<Int>): List<Int> {
       val listFinal = mutableListOf<Int>()
        var cpt = 0
        var values = 0

        for(i in s.indices){
            if(i == 0){
                cpt = s[i]

                values = s[i]
            }
            if(i > 0) {
                if (cpt == s[i]) {
                    values = values.plus(s[i])

                } else {
                    listFinal.add(values)
                    values = s[i]
                    cpt = s[i]

                }
            }
        }
        return listFinal
    }

    fun digitize(n:Long):IntArray {
        var reversed = 0
        var number = n.toInt()
        val array = mutableListOf<Int>()
        var i = 0
        if(number > 0) {
            while (number != 0) {
                val digit = n % 10
                reversed = (reversed * 10L + digit).toInt()
                number /= 10
                array.add(reversed)
                i++
            }

        } else {
            array.add(reversed)
        }
        return array.toIntArray()
    }


    fun findFirstSubString(string: String, subString: String): Int? =
        subString.takeIf { it.isNotEmpty() }?.let { string.indexOf(subString) }.takeIf { it != -1 }

    fun longest(s1:String, s2:String):String {
        val builder = StringBuilder()
        builder.append(s1).append(s2)
        val text = builder.toString().toCharArray().distinct()
        return text.sorted().joinToString("")
    }

    fun incrementString(str: String) : String {
        val text = str.split("^0+")

        return when {
            str.isEmpty() -> {
                1.toString()
            }
            text[1].isEmpty() -> {
                StringBuilder().append(text[0]).append(1).toString()
            }
            else -> {
                val values = text[1].toInt().plus(1)
                StringBuilder().append(text[0]).append(text[1]).toString()
            }
        }

    }


    /*
    fun longest(s1:String, s2:String):String {
    return (s1 + s2).toSortedSet().joinToString("")
}

    fun noSpace(x: String): String {

        return x.replace(" ","")
    }

    fun noSpace(x: String) = x.filterNot { it.isWhitespace() }

    fun countPositivesSumNegatives(input: Array<Int>?): Array<Int> {
        val list = Array(2){0}
        if(input?.isEmpty() == true){
            return emptyArray()
        }
        else {
            if (input != null) {
                for (number in input) {
                    if (number > 0) {
                        list[0] = 1 + list.first()
                    } else if (number < 0) {
                        list[1] = number + list[1]
                    }
                }
            } else return  emptyArray()
            return list
        }
    }

    fun countPositivesSumNegatives(input : Array<Int>?) : Array<Int> {
        if (input == null || input.isEmpty()) return emptyArray()
        val (positive, negative) = input.partition { it > 0 }
        return arrayOf(positive.count(), negative.sum())
    }

    fun countPositivesSumNegatives(input : Array<Int>?) : Array<Int> =
        if (input == null || input.isEmpty())
            arrayOf()
        else
            arrayOf(input.filter{ it > 0 }.count(), input.filter{ it < 0 }.sum())
}

fun countPositivesSumNegatives(input : Array<Int>?) : Array<Int> {

    if (input != null && input.isNotEmpty()) {
        var postiveCount = input.filter { it > 0 }.count();
        var negativeSum = input.filter { it < 0 }.sum();
        return arrayOf(postiveCount, negativeSum)
    }

    return arrayOf()*/

}