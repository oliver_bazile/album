package com.example.album.presentation.PhotosSrceen

import com.example.album.domain.model.Photos

data class GalleryViewState(
    var listPhotos: List<Photos> = emptyList(),
    var loading: Boolean = false,
    var errorPhotos: String = ""
)