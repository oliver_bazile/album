package com.example.album.presentation.di

import com.example.album.common.Constant
import com.example.album.data.network.AlbumApi
import com.example.album.data.repository.AlbumRepositoryImpl
import com.example.album.data.repository.AlbumSource
import com.example.album.data.repository.AlbumSourceImpl
import com.example.album.domain.repository.AlbumRepository
import com.example.album.domain.user_case.AllUserCase
import com.example.album.domain.user_case.GetAlbum
import com.example.album.domain.user_case.GetPhotos
import com.example.album.domain.user_case.GetUsers
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideAlbumRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constant.BASE_URL)
            .build()

    }

    @Provides
    @Singleton
    fun providersAlbumApi(retrofit: Retrofit): AlbumApi {
        return retrofit.create(AlbumApi::class.java)
    }

    @Provides
    @Singleton
    fun providerAlbumRemote(api: AlbumApi): AlbumSource {
        return AlbumSourceImpl(api)
    }

    @Singleton
    @Provides
    fun providerAlbumRepository(albumSource: AlbumSource): AlbumRepository {
        return AlbumRepositoryImpl(albumSource)
    }

    @Singleton
    @Provides
    fun providerUserCase(albumRepository: AlbumRepository): AllUserCase {
        return AllUserCase(
            getAlbum = GetAlbum(albumRepository),
            getPhotos = GetPhotos(albumRepository),
            getUsers = GetUsers(albumRepository)
        )
    }
}