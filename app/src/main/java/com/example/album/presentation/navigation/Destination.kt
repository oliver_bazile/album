package com.example.album.presentation.navigation

sealed class Destination (val route : String){
    object Album :Destination("Album")
    object Photos :Destination("Photos")
}