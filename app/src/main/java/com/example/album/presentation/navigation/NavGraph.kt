package com.example.album.presentation.navigation

import androidx.compose.animation.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.album.presentation.AlbumScreen.AlbumScreen
import com.example.album.presentation.AlbumScreen.AlbumViewModel
import com.example.album.presentation.PhotosSrceen.PhotosScreen
import com.example.album.presentation.PhotosSrceen.GalleryViewModel


@ExperimentalAnimationApi
@Composable
fun EnterAnimation(content: @Composable () -> Unit) {
    val density = LocalDensity.current
    AnimatedVisibility(
        visible = true,
        enter = slideInHorizontally(
            initialOffsetX = { with(density) { -40.dp.roundToPx() } }
        ) + expandHorizontally(
            expandFrom = Alignment.Start
        ) + fadeIn(
            // Fade in with the initial alpha of 0.3f.
            initialAlpha = 0.3f
        ),
        exit = slideOutHorizontally() + shrinkHorizontally() + fadeOut(),
        content = content,
        initiallyVisible = false
    )
}


@ExperimentalAnimationApi
@Composable
fun Navigation() {
    val navController = rememberNavController()
    val actions = remember(navController) { Actions(navController) }
    NavHost(navController = navController, startDestination = Destination.Album.route) {
        composable(route = Destination.Album.route) {
            val viewModel = hiltViewModel<AlbumViewModel>()
            AlbumScreen(viewModel = viewModel, actions.navigateToPhoto)
        }
        composable(
            route = "${Destination.Photos.route}/{${NavigationKeys.ALBUM_ID}}",
            arguments = listOf(navArgument(NavigationKeys.ALBUM_ID) {
                type = NavType.IntType
            })
        ) { backStackEntry ->
            val albumId = backStackEntry.arguments?.getInt(NavigationKeys.ALBUM_ID)
            val viewModel = hiltViewModel<GalleryViewModel>()
            EnterAnimation {
                PhotosScreen(albumId, viewModel)
            }
        }
    }
}

class Actions(private val navController: NavController) {
    val navigateToPhoto: (Int) -> Unit = { albumId: Int ->
        navController.navigate("${Destination.Photos.route}/$albumId")
    }
}

object NavigationKeys {
    const val ALBUM_ID = "albumId"
}