package com.example.album.presentation.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.album.R


val fontJosefin = FontFamily(
    listOf(
        Font(R.font.josefin_medium, FontWeight.Medium),
        Font(R.font.josefin_bold, FontWeight.Bold),
        Font(R.font.josefin_light, FontWeight.Light),
        Font(R.font.josefin_regular, FontWeight.Normal)
    )
)
val Typography = Typography(
    subtitle1= TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_bold, FontWeight.Normal)),
        fontWeight = FontWeight.Bold,
        fontSize = 16.sp
    ),
    body2 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_regular, FontWeight.Normal)),
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    body1 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_medium, FontWeight.Normal)),
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    h1 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_medium, FontWeight.Bold)),
        fontWeight = FontWeight.Bold,
        fontSize = 24.sp
    ),
    h2 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_medium, FontWeight.Normal)),
        fontWeight = FontWeight.Bold,
        fontSize = 16.sp
    ),
    h3 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_medium, FontWeight.Normal)),
        fontWeight = FontWeight.Bold,
        fontSize = 14.sp
    ),
    h4 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_medium, FontWeight.Normal)),
        fontWeight = FontWeight.Bold,
        fontSize = 12.sp
    ),
    h5 = TextStyle(
        color = Black,
        fontFamily = FontFamily(Font(R.font.josefin_medium, FontWeight.Normal)),
        fontWeight = FontWeight.Bold,
        fontSize = 10.sp
    )
)